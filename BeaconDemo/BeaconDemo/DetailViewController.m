//
//  DetailViewController.m
//  BeaconDemo
//
//  Created by Anthony Donker on 10-12-13.
//  Copyright (c) 2013 Anthony Donker. All rights reserved.
//

#import "DetailViewController.h"
#import "PlayAudio.h"

@interface DetailViewController ()
@property (nonatomic, weak) IBOutlet UIButton *meerButton;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIImageView *afbeeldingView;
@end

@implementation DetailViewController {
    NSString *audioFile;
}

//---------------------------------------------------------------------------------------------
#pragma mark - View lifecycle
//---------------------------------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:FALSE animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:63/255.0f green:71/255.0f blue:79/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor redColor]];
    
    self.meerButton.layer.cornerRadius = 5.0f;
    self.meerButton.titleLabel.font = [UIFont fontWithName:@"Rijksmuseum-Normal" size:21.0f];
    
    self.textView.font = [UIFont fontWithName:@"Rijksmuseum-Normal" size:14.0f];

    NSString *textTitle;
    NSString *deText;
    NSString *buttonTitle;
    NSString *imageFile;
    switch (self.detailType) {
        case RijksDetailNachtwacht: {
            audioFile = @"nachtwacht";
            textTitle = @"De Nachtwacht, Rembrandt van Rijn, 1638";
            buttonTitle = @"Meer over Rembrandt van Rijn";
            deText = @"\n\nIn 1638 besloot een groep schutters zichzelf te laten vereeuwigen door hun wijkgenoot Rembrandt van Rijn die aan de Breestraat woonde, niet ver van de Kloveniersdoelen. Het motief of het eigenlijke onderwerp van het grote doek staat kort beschreven in het latere familiealbum van Banning Cocq, naast een kleine aquarel van het schilderij; de heren dragen hier hun chique naam als landjonker: Schets van de schilderije op de groote Sael van de Cleveniers Doelen, daerinne de Jonge Heer van Purmerlandt als Capiteijn, geeft last aen zijnen Lieutenant, de Heer van Vlaerdingen, om sijn Compaignie burgers te doen marcheren. ";
            imageFile = @"nachtwacht";
            self.navigationItem.title = @"Rembrandt van Rijn";
        }
            break;
        case RijksDetailVermeer: {
            audioFile = @"melkmeisje";
            textTitle = @"Het Melkmeisje, Johannes Vermeer, ca. 1660";
            deText = @"\n\nGeheel verdiept in haar werk schenkt een dienstmeisje melk in. Behalve de witte melkstraal lijkt niets te bewegen. Die alledaagse handeling balde Vermeer samen tot een indrukwekkend schilderij – als een beeld staat de figuur vrij in de lichte ruimte. Vermeer had oog voor hoe het licht in honderden kleurige puntjes over de voorwerpen speelt. ";
            buttonTitle = @"Meer over Johannes Vermeer";
            imageFile = @"melkmeisje";
            self.navigationItem.title = @"Johannes Vermeer";
        }
            break;
        case RijksDetailVincent: {
            audioFile = @"zelfportret";
            textTitle = @"Zelfportret, Vincent van Gogh, 1887";
            deText = @"\n\nNadat hij van zijn broer Theo had gehoord over de nieuwe kleurige Franse schilderkunst, ging Vincent in 1886 in Parijs wonen. Al snel probeerde hij de Franse stijl uit op een aantal zelfportretten. Dit deed hij vooral om de kosten voor een model uit te ";
            buttonTitle = @"Meer over Vincent van Gogh";
            imageFile = @"zelfportret";
            self.navigationItem.title = @"Vincent van Gogh";
        }
            break;
        default:
            audioFile = @"nachtwacht";
    }

    NSDictionary *titleAttrs = @{NSFontAttributeName : [UIFont fontWithName:@"Rijksmuseum-Bold" size:14.0f]};
    NSDictionary *textAttrs = @{NSFontAttributeName : [UIFont fontWithName:@"Rijksmuseum-Normal" size:14.0f]};
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:textTitle attributes:titleAttrs];
    NSAttributedString *textString = [[NSAttributedString alloc] initWithString:deText attributes:textAttrs];
    [titleString appendAttributedString:textString];
    self.textView.attributedText = titleString;
    [self.meerButton setTitle:buttonTitle forState:UIControlStateNormal];
    self.afbeeldingView.image = [UIImage imageNamed:imageFile];
    
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[PlayAudio sharedPlayer] setFile:audioFile];
    [[PlayAudio sharedPlayer] play];
    
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[PlayAudio sharedPlayer] stop];
}
//---------------------------------------------------------------------------------------------
#pragma mark - Memory
//---------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
