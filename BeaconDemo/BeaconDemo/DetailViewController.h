//
//  DetailViewController.h
//  BeaconDemo
//
//  Created by Anthony Donker on 10-12-13.
//  Copyright (c) 2013 Anthony Donker. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    RijksDetailTypeNone,
    RijksDetailVincent,
    RijksDetailVermeer,
    RijksDetailNachtwacht
} RijksDetailType;

@interface DetailViewController : UIViewController
@property (nonatomic, assign) NSInteger detailType;
@end
