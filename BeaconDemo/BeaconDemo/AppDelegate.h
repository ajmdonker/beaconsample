//
//  AppDelegate.h
//  BeaconDemo
//
//  Created by Anthony Donker on 10-12-13.
//  Copyright (c) 2013 Anthony Donker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
