//
//  PlayAudio.h
//  Mastermovies
//
//  Created by Anthony on 24-02-11.
//  Copyright 2011 Anthony Donker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface PlayAudio : NSObject <AVAudioPlayerDelegate> {

	AVAudioPlayer *audioPlayer;
}
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

+(PlayAudio *)sharedPlayer;
-(void)play;
-(void)stop;
-(void)setVolume:(float)volume;
-(void)setFile:(NSString *)file;

@end
