//
//  RootViewController.m
//  BeaconDemo
//
//  Created by Anthony Donker on 10-12-13.
//  Copyright (c) 2013 Anthony Donker. All rights reserved.
//

#import "RootViewController.h"
#import "DetailViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>

static NSString *kEstimoteUUID          = @"B9407F30-F5F8-466E-AFF9-25556B57FE6D";
static NSString *kJaaleeUUID            = @"EBEFD083-70A2-47C8-9837-E7B5634DF524";
static NSString *kEstimoteIdentifier    = @"com.pandapps.estimotebeacon";
static NSString *kJaaLeeIdentifier      = @"com.pandapps.jaaleebeacon";

@interface RootViewController () <CLLocationManagerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) NSMutableDictionary *dRangedBeacons;
@property (nonatomic, strong) NSArray *aBeacons;
@property (nonatomic, weak) IBOutlet UIButton *startButton;
@property (nonatomic, weak) IBOutlet UIButton *proximityBytton;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation RootViewController {
    CLLocationManager *_locationManager;
    CLProximity checkedProximity;
    RijksDetailType selectedType;
    RijksDetailType currentType;
}



//---------------------------------------------------------------------------------------------
#pragma mark - View lifecycle
//---------------------------------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
    checkedProximity = CLProximityNear;
    [self setupBeacons];
    
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    self.startButton.hidden = FALSE;
    [self.activityIndicator stopAnimating];
    currentType = RijksDetailTypeNone;
    [self stopRangingBeacons];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}


//---------------------------------------------------------------------------------------------
#pragma mark - Custom methods
//---------------------------------------------------------------------------------------------
-(void)setupBeacons {

    self.dRangedBeacons = [NSMutableDictionary new];

    self.aBeacons = @[@{@"major" : @56064, @"minor" : @5120, @"UUID" : kJaaleeUUID,     @"type" : [NSNumber numberWithInteger:RijksDetailVermeer]},
                      @{@"major" : @55979, @"minor" : @9608, @"UUID" : kEstimoteUUID,   @"type" : [NSNumber numberWithInteger:RijksDetailVincent]},
                      
                      // deze kun jij wijzigen, Emiel :-) (nb: JaaLee staat er 2x omdat ik niet meer weet welke jij nu had)
                      @{@"major" : @31662, @"minor" : @22580, @"UUID" : kEstimoteUUID,   @"type" : [NSNumber numberWithInteger:RijksDetailVincent]},
                      @{@"major" : @52236, @"minor" : @54294, @"UUID" : kEstimoteUUID,   @"type" : [NSNumber numberWithInteger:RijksDetailNachtwacht]},
                      @{@"major" : @53248, @"minor" : @0, @"UUID" : kJaaleeUUID,   @"type" : [NSNumber numberWithInteger:RijksDetailVermeer]}];
    
    
    [self setProximityByttonTitle];
}


-(BOOL)isRangingPossible {
    CBCentralManager *central = [CBCentralManager new];
    if (![CLLocationManager isRangingAvailable] || [central state] == CBCentralManagerStatePoweredOff) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to start" message:@"iBeacons support not available on this device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return FALSE;
    }
    return TRUE;
}
//---------------------------------------------------------------------------------------------
#pragma mark - Beacons stuff
//---------------------------------------------------------------------------------------------

-(IBAction)toggleProximity:(id)sender {
    if (checkedProximity == CLProximityImmediate) {
        checkedProximity = CLProximityNear;
    } else {
        checkedProximity = CLProximityImmediate;
    }
    
    [self setProximityByttonTitle];
}



-(void)setProximityByttonTitle {
    if (checkedProximity == CLProximityImmediate) {
        [self.proximityBytton setTitle:@"proximity: immediate" forState:UIControlStateNormal];
    } else {
        [self.proximityBytton setTitle:@"proximity: near" forState:UIControlStateNormal];
    }
    
}



-(IBAction)startRangingBeacons:(id)sender {
//    selectedType = RijksDetailNachtwacht;
//    [self displayViewWithIdentifier:@"detailView2"];
//    return;
//    
    if (![self isRangingPossible]) {
        return;
    }
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    self.startButton.hidden = TRUE;
    [self.activityIndicator startAnimating];

    
    // estimotes
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:kEstimoteUUID];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:kEstimoteIdentifier];
    region.notifyOnEntry = YES;
    region.notifyOnExit = YES;
    
    // JaaLee 1
    NSUUID *jaaleeUUID = [[NSUUID alloc] initWithUUIDString:kJaaleeUUID];
    CLBeaconRegion *jaaleeRegion1 = [[CLBeaconRegion alloc] initWithProximityUUID:jaaleeUUID identifier:kJaaLeeIdentifier];
    jaaleeRegion1.notifyOnEntry = YES;
    jaaleeRegion1.notifyOnExit = YES;
    jaaleeRegion1.notifyEntryStateOnDisplay = YES;
    
    [_locationManager startRangingBeaconsInRegion:jaaleeRegion1];
    [_locationManager startRangingBeaconsInRegion:region];
}

-(void)checkNearestBeacon {
    // do nothing if both keys do not exist
    if (self.dRangedBeacons[kJaaLeeIdentifier] == nil || self.dRangedBeacons[kEstimoteIdentifier] == nil) {
        return;
    }
    
    // enumerate through found beacons
    NSMutableArray *beaconsArray = [@[] mutableCopy];
    NSLog(@"-----------------------------------------------------------------------------");
    [self.dRangedBeacons enumerateKeysAndObjectsUsingBlock:^(NSString *identifierKey, NSArray *beacons, BOOL *stop) {
        [beacons enumerateObjectsUsingBlock:^(CLBeacon *beacon, NSUInteger idx, BOOL *stop) {
            beaconsArray[beaconsArray.count] = beacon;
            NSString *beaconModel = @"unknown";
            if ([beacon.proximityUUID.UUIDString isEqualToString:kJaaleeUUID]) {
                beaconModel = @"JaaLee";
            }
            if ([beacon.proximityUUID.UUIDString isEqualToString:kEstimoteUUID]) {
                beaconModel = @"Estimote";
            }
            NSLog(@"Beacon UUID:%@ (%@) - Major: %li - Minor: %li",beacon.proximityUUID.UUIDString, beaconModel, (long)beacon.major.integerValue,(long)beacon.minor.integerValue);
        }];
    }];
    
    // do we have a beacon in the nearest vicinity?
    __block BOOL bBeaconIsNearest = FALSE;
    __block RijksDetailType detailType;
    if (beaconsArray.count > 0) {
        [beaconsArray enumerateObjectsUsingBlock:^(CLBeacon *beacon, NSUInteger idx, BOOL *stop) {
            // is beacon in our array of beacons?
            [self.aBeacons enumerateObjectsUsingBlock:^(NSDictionary *refBeacon, NSUInteger idx, BOOL *stop) {
                if (beacon.major.integerValue == [refBeacon[@"major"] integerValue] &&
                    beacon.minor.integerValue == [refBeacon[@"minor"] integerValue] &&
                    [beacon.proximityUUID.UUIDString isEqualToString:refBeacon[@"UUID"]] &&
                    ([self beaconProximityInCheckedRange:beacon.proximity])) {
                    bBeaconIsNearest = TRUE;
                    detailType = [refBeacon[@"type"] integerValue];
                    *stop = TRUE;
                }
            }];
            
            if (bBeaconIsNearest) {
                *stop = TRUE;
                selectedType = detailType;
                [self displayViewWithIdentifier:@"detailView2"];
            }
        }];
    }

    self.dRangedBeacons = [NSMutableDictionary new];
}


-(void)stopRangingBeacons {
    if (_locationManager.rangedRegions) {
        [_locationManager.rangedRegions enumerateObjectsUsingBlock:^(CLBeaconRegion *beaconRegion, BOOL *stop) {
            [_locationManager stopRangingBeaconsInRegion:beaconRegion];
        }];
    }
}




-(BOOL)beaconProximityInCheckedRange:(CLProximity)proximity {
    if (checkedProximity == CLProximityNear) {
        if (proximity != CLProximityUnknown && (proximity == CLProximityNear || proximity == CLProximityImmediate)) {
            return TRUE;
        }
    };
    
    if (checkedProximity == CLProximityImmediate) {
        if (proximity != CLProximityUnknown && proximity == CLProximityImmediate) {
            return TRUE;
        }
    };
    return FALSE;
    
}



-(void)displayViewWithIdentifier:(NSString *)identifier {
    
    // already on current or no valid segue Identifier
    if (selectedType == currentType) {
        return;
    }
//    if ([currentSegue isEqualToString:identifier] || identifier == nil) {
//        return;
//    }
    
    // new segue found!
    currentType = selectedType;
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self performSegueWithIdentifier:identifier sender:self];
    
}
//------------------------------------------------------------------------------------------------------
#pragma mark - location manager delegate
//------------------------------------------------------------------------------------------------------

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
   // [self startRangingBeacons:nil];
    return;
    CLBeaconRegion *beaconRegion = (CLBeaconRegion *)region;
    if ([beaconRegion.identifier isEqualToString:@"com.pandapps.beacon"]) {
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        localNotif.alertBody = @"BEACON FOUND";
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    }
    NSLog(@"beacon found: %@",region);
}



-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
 //   [self stopRangingBeacons];
}


/*
 Create a dictionary of all regions and add its ranged beacons
 NOTE: even if there are NO beacons found, the delegate is called with the region
 like it's saying: NO beacons for region found
 */
-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    if (self.dRangedBeacons[region.identifier] == nil) {
        self.dRangedBeacons[region.identifier] = beacons;
    }
    [self checkNearestBeacon];
    
}

//---------------------------------------------------------------------------------------------
#pragma mark - storyboard
//---------------------------------------------------------------------------------------------
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    DetailViewController *detailVC = (DetailViewController *)segue.destinationViewController;
    detailVC.detailType = selectedType;

}

//---------------------------------------------------------------------------------------------
#pragma mark - memory
//---------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
