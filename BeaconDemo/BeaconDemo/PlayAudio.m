//
//  PlayAudio.m
//  Mastermovies
//
//  Created by Anthony on 24-02-11.
//  Copyright 2011 Anthony Donker. All rights reserved.
//

#import "PlayAudio.h"


//http://developer.apple.com/library/ios/#documentation/AudioVideo/Conceptual/MultimediaPG/UsingAudio/UsingAudio.html%23//apple_ref/doc/uid/TP40009767-CH2

static PlayAudio *_SharedPlayAudioInstance;

@implementation PlayAudio

@synthesize audioPlayer;


+(PlayAudio *)sharedPlayer {
	if (_SharedPlayAudioInstance == nil) {
		_SharedPlayAudioInstance = [[PlayAudio alloc] init];
	}
	return _SharedPlayAudioInstance;
}

-(id)init {
	if (self = [super init]) {
	}
	return self;
}


-(void)dealloc {
	_SharedPlayAudioInstance = nil;
}

-(void)play {
	[self.audioPlayer play];
}
-(void)stop {
	[self.audioPlayer stop];
}

-(void)setVolume:(float)volume
{
	self.audioPlayer.volume = volume;
    
}

-(void)setFile:(NSString *)file {
	if (self.audioPlayer != nil) {
		[audioPlayer stop];
	}
	NSString *soundFilePath =[[NSBundle mainBundle] pathForResource:file ofType: @"m4a"];
	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
	AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
	newPlayer.volume = 1.0f;
    newPlayer.delegate = self;
	[newPlayer prepareToPlay];
	self.audioPlayer = newPlayer;
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self.audioPlayer play];
}
	


@end
